package com.example.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.test.entity.Transaction;

public interface TransactionRepository extends MongoRepository<Transaction, Long> {

}
