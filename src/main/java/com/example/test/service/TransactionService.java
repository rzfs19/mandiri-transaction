package com.example.test.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.test.entity.Transaction;
import com.example.test.repository.TransactionRepository;

@Service
public class TransactionService {

	private final TransactionRepository transactionRepository;

	@Autowired
	public TransactionService(TransactionRepository transactionRepository) {
		this.transactionRepository = transactionRepository;
	}

	public List<Transaction> getAllTransactions() {
		return transactionRepository.findAll();
	}

	public Optional<Transaction> getTransactionById(Long id) {
		return transactionRepository.findById(id);
	}

	public Transaction createTransaction(Transaction transaction) {
		return transactionRepository.save(transaction);
	}

	public Transaction updateTransaction(Long id, Transaction transaction) {
		if (transactionRepository.existsById(id)) {
			transaction.setId(id);
			return transactionRepository.save(transaction);
		} else {
			// Handle error - transaction not found
			return null;
		}
	}

	public void deleteTransaction(Long id) {
		transactionRepository.deleteById(id);
	}

}
